
/*=================================
||          Owl Carousel
==================================*/
    $("#header-slider").owlCarousel({

        navigation : true, // Show next and prev buttons
        slideSpeed : 100,
        paginationSpeed : 400,
        singleItem: true,
        autoPlay: true,
        pagination: false,

        // "singleItem:true" is a shortcut for:
        // items : 1, 
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

    });

/*=================================
||          WOW
==================================*/
wow = new WOW(
    {
      boxClass:     'wow',      // default
      animateClass: 'animated', // default
      offset:       0,          // default
      mobile:       true,       // default
      live:         true        // default
    }
  )
wow.init();

/*=================================
||          Smooth Scrooling
==================================*/
    $(function() {
        $('a[href*=#]:not([href=#])').click(function() {
            if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: (target.offset().top - 9)//top navigation height
                    }, 1000);
                    return false;
                }
            }
        });
    });

    
/*====================================================================
            Navbar shrink script
======================================================================*/
$(document).ready(function() {
    $(window).scroll(function() {
        if ($(document).scrollTop() > 50) {
            $('nav').addClass('shrink');
        } 
        else {
            $('nav').removeClass('shrink');
        }
    });
});


$(document).ready(function(){
    $(window).scroll(function() {
        if ($(document).scrollTop() > 50) {
           // $("#logo").attr("src", "images/Logo_stick.png")
            $("#logo").attr("src", "mstile-150x150.png")
        }
        else {
           //  $("#logo").attr("src", "images/icons/logo.png")
             $("#logo").attr("src", "mstile-150x150.png")
        }
    });
});
/*=================================================================
            Load more button
===================================================================*/

$(document).ready(function () {
    $("#loadMenuContent").click(function(event) {
        
        $.get("php/ajax_menu.html", function(data){
            $('#moreMenuContent').append(data);
        });
        event.preventDefault();
        $(this).hide();
    }) ;
});

$(document).ready(function () {

    var $menuPricing = $('#menu-pricing');
    $menuPricing.mixItUp({
        selectors: {
            target: 'li'
        },
    });

    $(document).on('click', '[data-toggle="lightbox"]', function(event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });

});


/*=================================================
        Showing Icon in placeholder
=====================================================*/

$('.iconified').on('keyup', function() {
    var input = $(this);
    if(input.val().length === 0) {
        input.addClass('empty');
    } else {
        input.removeClass('empty');
    }
});

$('#direccion').on('click', function () {
    window.open("https://goo.gl/maps/UGZGFqAWHvdR69ah7",'__blank');
});


$('.whatsapp').click(function () {
    var ua = navigator.userAgent;
    var iOS = !!ua.match(/iPad/i) || !!ua.match(/iPhone/i);
    var Android = !!ua.match(/Android/i);
    var Mobile = !!ua.match(/Mobi/i);
    var Mac = !!ua.match(/Macintosh/i);
    var  $this=this;
    var numero=$this.getAttribute('data-telefono');
    var link;
    if (Mobile) {
        link = 'whatsapp://send/?phone=' + encodeURIComponent(numero) + '&text=' + encodeURIComponent("");
    } else {
        link = 'https://web.whatsapp.com/send?phone=' + encodeURIComponent(numero) + '&text=' + encodeURIComponent("");
    }

    if (link) {
        if (Mobile) {
            window.location = link;
        } else {
            window.open(link,'__blank');
        }
    }

});
/*=========================================================
                Scroll  Speed
=======================================================*/

$(function() {  
    jQuery.scrollSpeed(100, 1000);
});


$('#contact-submit').click(function (e) {

    //stop the form from being submitted
    e.preventDefault();

    /* declare the variables, var error is the variable that we use on the end
    to determine if there was an error or not */
    var error = false;
    var name = $('#nombre').val();
    var email = $('#correo').val();
    var subject = $('#subject').val();
    var message = $('#message').val();

    /* in the next section we do the checking by using VARIABLE.length
    where VARIABLE is the variable we are checking (like name, email),
    length is a JavaScript function to get the number of characters.
    And as you can see if the num of characters is 0 we set the error
    variable to true and show the name_error div with the fadeIn effect.
    if it's not 0 then we fadeOut the div( that's if the div is shown and
    the error is fixed it fadesOut.

    The only difference from these checks is the email checking, we have
    email.indexOf('@') which checks if there is @ in the email input field.
    This JavaScript function will return -1 if no occurrence have been found.*/
    if (name.length == 0) {
        var error = true;
        $('#nombre').css("border-color", "#D8000C");
    } else {
        $('#nombre').css("border-color", "#666");
    }
    if (email.length == 0 || email.indexOf('@') == '-1') {
        var error = true;
        $('#correo').css("border-color", "#D8000C");
    } else {
        $('#correo').css("border-color", "#666");
    }
    if (subject.length == 0) {
        var error = true;
        $('#subject').css("border-color", "#D8000C");
    } else {
        $('#subject').css("border-color", "#666");
    }
    if (message.length == 0) {
        var error = true;
        $('#message').css("border-color", "#D8000C");
    } else {
        $('#message').css("border-color", "#666");
    }

    //now when the validation is done we check if the error variable is false (no errors)
    if (error == false) {
        //disable the submit button to avoid spamming
        //and change the button text to Sending...
        $('#contact-submit').attr({
            'disabled': 'false',
            'value': 'Enviando...'
        });

        /* using the jquery's post(ajax) function and a lifesaver
        function serialize() which gets all the data from the form
        we submit it to send_email.php */
        $.post("sendmail.php", $("#contact-form").serialize(), function (result) {
            //and after the ajax request ends we check the text returned
            if (result == 'sent') {
                //if the mail is sent remove the submit paragraph
                $('#cf-submit').remove();
                $('#mail-fail').remove();
                //and show the mail success div with fadeIn
                $('#mail-success').fadeIn(500);
                $('#contact-submit').removeAttr('disabled').attr('value', 'Send The Message');
            } else {
                //show the mail failed div
                $('#mail-fail').fadeIn(500);
                //re enable the submit button by removing attribute disabled and change the text back to Send The Message
                $('#contact-submit').removeAttr('disabled').attr('value', 'Send The Message');
            }
        });
    }
});